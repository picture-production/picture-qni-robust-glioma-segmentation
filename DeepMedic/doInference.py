from clize import run
import logging
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
import torch
if torch.cuda.is_available():
    device_id = "cuda:0"
    logging.info('Found GPU, using cuda device 0: '+ str(torch.cuda.get_device_name(0)))
else:
    device_id = 'cpu'
    logging.warning("\x1b[31;1m"+'No GPU found, using CPU. Will be slow!!\x1b[0m (approx. 10 min. for 1 scan on a 12 core CPU)') #weird chars add color
import os
import subprocess
import shutil


import ants
from HD_BET.run import run_hd_bet
import tempfile

def do_preprocessing(t1ce, t1, t2, flair, mni=False, wdir_postfix=''):
    logging.info('''01/10) Apply n4-bias correction''')
    scan = {'T1c':t1ce, 'T1w':t1, 'T2w': t2, 'FLR':flair}
    imgs = {}
    for mod in ['T1c','T1w','T2w','FLR']:
        if scan[mod]:
            if mod in ['FLR']:
                imgs[mod] = ants.image_read(scan[mod])
            else:
                out_fname = os.path.split(scan[mod])[0]+'/wdir/'+wdir_postfix+'/bias_corrected'+'/'+os.path.split(scan[mod])[1]
                if not(os.path.isfile(out_fname)):
                    os.makedirs(os.path.split(out_fname)[0], exist_ok=True)
                    tst = subprocess.call(['/workspace/install/bin/N4BiasFieldCorrection','-i', scan[mod],'-o', out_fname])
                    if tst == 1:
                        print(mod + ' n4BiasCorrection failed, using native image: ')
                        print(out_fname)
                        ants.image_read(scan[mod])
                    else:
                        imgs[mod] = ants.image_read(out_fname)
                else:
                    print(mod + ' re-using existing n4BiasCorrection')
                    imgs[mod] = ants.image_read(out_fname)


    logging.info('''02/10) Perform registration to atlas''')
    transforms = {}
    if not(mni):
        atlas = ants.image_read('/sri24/sri24/spgr_unstrip.nii')
    else:
        atlas = ants.image_read('/mni/MNI152_T1_1mm.nii.gz')
    for mod in imgs.keys():
        if mod == 'T1c':
            o_fname = os.path.split(scan[mod])[0]+'/wdir/'+wdir_postfix+'/transforms'+'/'+mod+'_TKI.mat'
            if not(os.path.isfile(o_fname)):
                transforms[mod+'_TKI'] = ants.registration(fixed=atlas , moving=imgs[mod], type_of_transform='Affine' )

                os.makedirs(os.path.split(o_fname)[0], exist_ok=True)
                shutil.copy(transforms[mod+'_TKI']['fwdtransforms'][0], o_fname)
            else:
                print('reusing existing registration: '+o_fname)
                transforms[mod+'_TKI'] = {}
            transforms[mod+'_TKI']['fwdtransforms'] = [o_fname]
        else:
            if scan[mod]:
                o_fname = os.path.split(scan[mod])[0]+'/wdir/'+wdir_postfix+'/transforms'+'/'+mod+'_T1c.mat'
                if not(os.path.isfile(o_fname)):
                    transforms[mod+'_T1c'] = ants.registration(fixed=imgs['T1c'] , moving=imgs[mod], type_of_transform='Rigid' )
                    os.makedirs(os.path.split(o_fname)[0], exist_ok=True)
                    shutil.copy(transforms[mod+'_T1c']['fwdtransforms'][0], o_fname)
                else:            
                    transforms[mod+'_T1c'] = {}
                transforms[mod+'_T1c']['fwdtransforms'] = [o_fname]
            else:
                print('No '+mod+' found, using all zero volume instead.')
    logging.info('''03/10) Apply registration to atlas''')
    imgs_tki = {}
    for mod in imgs.keys():
        if mod == 'T1c':
            imgs_tki[mod] = ants.apply_transforms(fixed=atlas, moving=imgs[mod], 
                                  transformlist=[transforms['T1c_TKI']['fwdtransforms'][0]])       
        else:
            imgs_tki[mod] = ants.apply_transforms(fixed=atlas, moving=imgs[mod], 
                                  transformlist=[
                                                transforms['T1c_TKI']['fwdtransforms'][0],
                                                transforms[mod+'_T1c']['fwdtransforms'][0]])
        os.makedirs(os.path.dirname(os.path.split(scan[mod])[0]+'/wdir/'+wdir_postfix+'/nii_data_tki'+'/'+os.path.split(scan[mod])[1]),exist_ok=True)
        ants.image_write(imgs_tki[mod],os.path.split(scan[mod])[0]+'/wdir/'+wdir_postfix+'/nii_data_tki'+'/'+os.path.split(scan[mod])[1])
    logging.info('''04/10) Skullstrip''')
    scan_fname = os.path.split(scan['T1c'])[0]+'/wdir/'+wdir_postfix+'/nii_data_tki'+'/'+os.path.split(scan['T1c'])[1]

    #    T1c_fname = scan['T1c'][0].replace('nii_data','nii_final')
    mask_fname = scan_fname.replace('nii_data_tki/','nii_data_tki/brain_mask_')
    if not(os.path.isfile(mask_fname)):
        run_hd_bet([scan_fname], [mask_fname],device=device_id)
        import torch
        torch.cuda.empty_cache() 
    mask_fname = mask_fname[:-7] + "_mask.nii.gz"
    print('mask_fname: ' + mask_fname)
    mask = ants.image_read(mask_fname)
   
    logging.info('''5&6/10) Apply skullstrip and normalization''')
    #print('normalization')
    outputs = {}
    for mod in ['T1c','T1w','T2w','FLR']:
        if mod in imgs_tki.keys():
            imgs_tki[mod] = imgs_tki[mod] * mask

            logical_mask = mask.numpy().astype(bool)
            mean = imgs_tki[mod].numpy()[logical_mask].mean()
            std = imgs_tki[mod].numpy()[logical_mask].std()
            imgs_tki[mod] = (imgs_tki[mod] - mean) / std

            o_fname = os.path.split(scan[mod])[0]+'/wdir/'+wdir_postfix+'/nii_final'+'/'+os.path.split(scan[mod])[1]

            os.makedirs(os.path.split(o_fname)[0], exist_ok=True)
            ants.image_write(imgs_tki[mod], o_fname)
            outputs[mod] = o_fname
        else:
            o_fname = os.path.split(scan['T1c'])[0]+'/wdir/'+wdir_postfix+'/nii_final'+'/empty_'+mod+os.path.split(scan['T1c'])[1]
            os.makedirs(os.path.split(o_fname)[0], exist_ok=True)
            imgs_tki[mod] = imgs_tki['T1c'] * 0
            ants.image_write(imgs_tki[mod], o_fname)
            outputs[mod] = o_fname
    return scan, transforms, outputs

def _do_segmentation(scan, transforms, outputs,wdir_postfix):
    logging.info("07/10) Load segmentation network")
    os.chdir(os.path.split(scan['T1c'])[0]+'/wdir/'+wdir_postfix)
    if os.path.isdir('/workspace/model/test_config/DeepMedic_workdir/'):
        shutil.rmtree('/workspace/model/test_config/DeepMedic_workdir/')
    os.makedirs('/workspace/model/test_config/DeepMedic_workdir/',exist_ok=True)
    for mod,img in outputs.items():
        shutil.copy(img, '/workspace/model/test_config/DeepMedic_workdir/'+mod+'.nii.gz')
    tst = subprocess.call(("/deepmedic/deepMedicRun -model /workspace/model/modelConfig.cfg -test /workspace/model/test_config/testConfig.cfg -load /workspace/model/deepMedic.trainSessionDm.final.2022-01-12.06.52.50.467054.model.ckpt -dev "+device_id.replace("cuda:0","cuda")).split(' '))
    #if not(tst==1):
    #    print('DeepMedic Failed :(')
    #    segmentations = None
    #else:
    segmentations = {'segmentation':'/workspace/model/test_config/output/predictions/testSessionDm/predictions/DMprediction_Segm.nii.gz'}
    
    seg_native = ants.apply_transforms(fixed=ants.image_read(scan['T1c']), moving=ants.image_read(segmentations['segmentation']),transformlist = transforms['T1c_TKI']['fwdtransforms'], interpolator='nearestNeighbor', whichtoinvert = [True])
    ants.image_write(seg_native, segmentations['segmentation'].replace('.nii.gz','_native.nii.gz'))
    segmentations['segmentation_native'] = segmentations['segmentation'].replace('.nii.gz','_native.nii.gz')
    return segmentations

def do_segmentation(t1ce, t1=None, t2=None, flair=None, *, remove_intermediate_files = False,mni=False, wdir_postfix='random'):
    """Performs automatic glioma segmentation using nvNet, trained using sparsified training on BRATS and PICTURE data. 
    See https://gitlab.com/picture/ for details.

    :param t1ce:  Nifti (.nii.gz) file containing T1w+contrast agent scan of glioma patient (Required)
    :param t1:    Nifti (.nii.gz) file containing T1w                scan of glioma patient (Optional)
    :param t2:    Nifti (.nii.gz) file containing T2w                scan of glioma patient (Optional)
    :param flair: Nifti (.nii.gz) file containing flair              scan of glioma patient (Optional)
    :param remove_intermediate_files: delete working directory containing transforms, preprocessed images etc.
    :param wdir_postfix: Sub-directory of the working-directory. Defaults to a random string to ensure a unique working directory if ran multiple times for different patients.

    """
    assert os.path.isfile(t1ce), "t1ce needs to be provided to be able to run"
    if wdir_postfix=='random':
        wdir_postfix = os.path.split(tempfile.mkdtemp())[1]
    logging.info('working dir:      '+'./wdir/'+wdir_postfix)
    scan, transforms, outputs = do_preprocessing(t1ce, t1, t2, flair, mni, wdir_postfix)
    segmentations = _do_segmentation(scan, transforms, outputs, wdir_postfix)
    logging.info('DONE!!! Saved segmentations to: ')
    for segType, seg in segmentations.items():
        dirname,fname = os.path.split(scan['T1c'])
        out_fname = os.path.join(dirname,segType+'_'+fname)
        shutil.copy(seg, out_fname)
        logging.info(out_fname)


    if remove_intermediate_files:
        shutil.rmtree(os.path.split(scan['T1c'])[0]+'/wdir/'+wdir_postfix)

if __name__ == '__main__':
    run(do_segmentation)


