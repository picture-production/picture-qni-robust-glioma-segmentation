ARG PYTORCH_IMAGE=nvcr.io/nvidia/pytorch:21.12-py3
FROM ${PYTORCH_IMAGE} as builder
COPY ./installANTs.sh ./installANTs.sh
RUN chmod +x ./installANTs.sh && ./installANTs.sh && rm -r /workspace/ANTs && rm -r /workspace/build



FROM ${PYTORCH_IMAGE} as base
COPY --from=builder  /workspace/install/bin/N4BiasFieldCorrection /workspace/install/bin/N4BiasFieldCorrection
# Install preprocessing tools
RUN mkdir /sri24/ && wget -q https://www.nitrc.org/frs/download.php/4502/sri24_anatomy_unstripped_nifti.zip && unzip sri24_anatomy_unstripped_nifti.zip -d /sri24/ && rm sri24_anatomy_unstripped_nifti.zip
RUN git clone https://github.com/MIC-DKFZ/HD-BET 
RUN cd HD-BET && pip install -e . && cd ..
RUN pip install antspyx numpy clize
RUN python -c "from HD_BET.utils import maybe_download_parameters; [maybe_download_parameters(i) for i in range(5)]"

RUN pip install --upgrade git+https://github.com/nanohanno/hiddenlayer.git@bugfix/get_trace_graph#egg=hiddenlayer
COPY ./nnunet-master ./nnunet-master
RUN cd ./nnunet-master/ && pip install -e . && cd ..
RUN pip install git+https://github.com/MIC-DKFZ/batchgenerators.git@1d240bd4770bd403cf4c6da9b307d649df7f7e89
ENV nnUNet_raw_data_base="/mnt/data/nnUnet-data"
ENV nnUNet_preprocessed="/mnt/data/nnUNet_preprocessed"
ENV RESULTS_FOLDER="/mnt/data/nnUNet_trained_models"

RUN mkdir /mni/ && wget -q https://raw.githubusercontent.com/Washington-University/HCPpipelines/master/global/templates/MNI152_T1_1mm.nii.gz -O /mni/MNI152_T1_1mm.nii.gz

COPY ./model/ /mnt/data/nnUNet_trained_models/nnUNet/
COPY ./doInference.py /workspace/doInference.py
ENTRYPOINT ["python", "/workspace/doInference.py"]
