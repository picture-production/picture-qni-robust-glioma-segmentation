# Picture Ucl Glioma Segmentation

This repository contains the three models trained for 3 class automatic glioma segmentation on the BraTS and picture datasets using sparsified training.

The best-performing nnUNet model is also implemented in the new picture-nnunet-package available on https://gitlab.com/picture-production/picture-nnunet-package which also adds support for post-operative segmentation.

## Installation:
Install docker: https://docs.docker.com/get-docker/ (or alternatively [podman](https://podman.io)/[singularity](https://singularity.lbl.gov/archive/docs/v2-3/index.html) if you don't have root privileges)

### GPU support:
(Optional) For docker: install [nvidia-docker](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)

### Inference times (seconds)*:

|           | Preprocessing |     | Inference |     | Total |     |
|-----------|---------------|-----|-----------|-----|-------|-----|
|           | CPU           | GPU | CPU       | GPU | CPU   | GPU |
| DeepMedic | 390           | 202 | 170       | 32  | 560   | 234 |
| nVNet     | 390           | 202 | 17        | 15  | 407   | 217 |
| nnUNet    | 390           | 202 | 1860      | 72  | 2250  | 274 |

*Based on 12-core AMD 3900X, and nvidia RTX 3090

## Example:
```Bash
# nnunet:
docker run -it --gpus all -v /mnt/data/sub-xxx/preop/anat:/wdir/ registry.gitlab.com/picture-production/picture-qni-robust-glioma-segmentation/nnunet /wdir/preop_space-NativeT1c.nii.gz /wdir/preop_space-NativeT1w.nii.gz /wdir/preop_space-NativeT2w.nii.gz /wdir/preop_space-NativeFLR.nii.gz

# nvnet:
docker run -it --gpus all -v /mnt/data/sub-xxx/preop/anat:/wdir/ registry.gitlab.com/picture-production/picture-qni-robust-glioma-segmentation/nvnet /wdir/preop_space-NativeT1c.nii.gz /wdir/preop_space-NativeT1w.nii.gz /wdir/preop_space-NativeT2w.nii.gz /wdir/preop_space-NativeFLR.nii.gz

# deepmedic:
docker run -it --gpus all -v /mnt/data/sub-xxx/preop/anat:/wdir/ registry.gitlab.com/picture-production/picture-qni-robust-glioma-segmentation/deepmedic /wdir/preop_space-NativeT1c.nii.gz /wdir/preop_space-NativeT1w.nii.gz /wdir/preop_space-NativeT2w.nii.gz /wdir/preop_space-NativeFLR.nii.gz
```
Only the t1ce nifti is required, t1w, t2w and flair are optional.

All algorithms are called using a python script with the same interface:
```Bash
# run:
docker run -it registry.gitlab.com/picture-production/picture-qni-robust-glioma-segmentation/deepmedic --help

# output:
Usage: ./doInference.py [OPTIONS] t1ce [t1] [t2] [flair]

Performs automatic glioma segmentation using nvNet, trained using sparsified training on BRATS and PICTURE data.  See https://gitlab.com/picture/ for details.

Arguments:
  t1ce                          Nifti (.nii.gz) file containing T1w+contrast agent scan of glioma patient (Required)
  t1                            Nifti (.nii.gz) file containing T1w                scan of glioma patient (Optional)
  t2                            Nifti (.nii.gz) file containing T2w                scan of glioma patient (Optional)
  flair                         Nifti (.nii.gz) file containing flair              scan of glioma patient (Optional)

Options:
  --remove-intermediate-files   delete working directory containing transforms, preprocessed images etc.
  --wdir-postfix=STR            Sub-directory of the working-directory. Defaults to a random string to ensure a unique working directory if ran multiple times for different patients. (default: random)

Other actions:
  -h, --help                    Show the help
</output>
```

The script returns a wdir/tmp$RandomString subfolder in the folder containing the t1ce containing the outputs of the intermediate steps:
 - bias_corrected:  Bias corrected images
 - nii_data_tki:    Skullstripped images in SRI24 space
 - transforms:      Transformation files used for the registration
 - nii_final:       The final preprocessed images used as input for the neural-network.

Next to the working directory wdir, the segmentation in native patient space and SRI24 space are returned in the main folder containing the t1ce.
For the output segmentations the t1ce filename is prepended with segmentation_ and segmentation_native_.
